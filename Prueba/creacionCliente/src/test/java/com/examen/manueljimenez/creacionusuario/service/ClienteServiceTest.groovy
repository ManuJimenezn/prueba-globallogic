package com.examen.manueljimenez.creacionusuario.service

import com.examen.manueljimenez.creacionusuario.entity.UsuarioEntity
import com.examen.manueljimenez.creacionusuario.exception.BadRequestException
import com.examen.manueljimenez.creacionusuario.exception.ConflictException
import com.examen.manueljimenez.creacionusuario.model.Phone
import com.examen.manueljimenez.creacionusuario.model.Usuario
import com.examen.manueljimenez.creacionusuario.repository.ContactoRepository
import com.examen.manueljimenez.creacionusuario.repository.UsuarioRepository

import spock.lang.Specification
import spock.lang.Unroll

@Unroll
class ClienteServiceTest extends Specification {

	void"ConflictException correo existente"() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', name:'pepe', password: 'Welcome11')
			def usuarioEntity = new UsuarioEntity(email:'email@gmail.com')
			def usuarios = new ArrayList<UsuarioEntity>()
			usuarios.add(usuarioEntity)
			def repoUsuario = Spy(UsuarioRepository)
			repoUsuario.findAll() >> usuarios
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(ConflictException)
	}

	void"BadRequestException correo no valido"() {
		given:
			def usuario = new Usuario(email: 'email', name:'pepe', password: 'Welcome11')
			def repoUsuario = Spy(UsuarioRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(BadRequestException)
	}

	void"BadRequestException Password no valido"() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', name:'pepe', password: 'welcome1')
			def repoUsuario = Spy(UsuarioRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(BadRequestException)
	}

	void"BadRequestException Phone null o vacio"() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', name:'pepe', password: 'Welcome11')
			def contactos = new ArrayList<Phone>()
			usuario.setPhones(contactos)
			def repoUsuario = Spy(UsuarioRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(BadRequestException)
	}

	void"BadRequestException Phone telefono no cumple con el largo"() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', name:'pepe', password: 'Welcome11')
			def contacto = new Phone(number: 9636807698L,citycode: 11L, contrycode: 11L)
			def contactos = new ArrayList<Phone>()
			contactos.add(contacto)
			usuario.setPhones(contactos)
			def repoUsuario = Spy(UsuarioRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(BadRequestException)
	}

	void"BadRequestException nombre nulo o vacio "() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', password: 'Welcome11')
			def contacto = new Phone(number: 936807698L,citycode: 11L, contrycode: 11L)
			def contactos = new ArrayList<Phone>()
			contactos.add(contacto)
			usuario.setPhones(contactos)
			def repoUsuario = Spy(UsuarioRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			repoUsuario.count() >> 1
			def service = new ClienteService(usuarioRepository: repoUsuario)
		when:
			service.procesarUsuario(usuario)
		then:
			thrown(BadRequestException)
	}

	void"Usuario Exitoso"() {
		given:
			def usuario = new Usuario(email: 'email@gmail.com', name:'pepe', password: 'Welcome11')
			def contacto = new Phone(number: 936807698L,citycode: 11L, contrycode: 11L)
			def contactos = new ArrayList<Phone>()
			contactos.add(contacto)
			usuario.setPhones(contactos)
			def repoUsuario = Mock(UsuarioRepository)
			def repoContacto = Mock(ContactoRepository)
			def usuarios = new ArrayList<UsuarioEntity>()
			repoUsuario.findAll() >> usuarios
			repoUsuario.count() >> 1
			def service = new ClienteService(usuarioRepository: repoUsuario, contactoRepository : repoContacto)
		when:
			def usuarioCreado = service.procesarUsuario(usuario)
		then:
			usuarioCreado.getIdCliente()!=null
	}
}

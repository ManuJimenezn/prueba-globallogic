package com.examen.manueljimenez.creacionusuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

/**
 * Error Request (campos malos, faltan datos, etc).
 * 
 * @author Manuel Jimenez.
 */
@Getter
@ResponseStatus(code = HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor.
	 * 
	 * @param message message.
	 */
	public BadRequestException(String message) {
		super(message);
	}

}
package com.examen.manueljimenez.creacionusuario.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Clase que contiene las Constantes del proyecto.
 * 
 * @author Manuel Jimenez
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ConstanteUsuario {

	/**
	 * el valor del texto contenido es vacio..
	 */
	public static final String EMPTY = "";

	/**
	 * BADREQUEST_MESSAGE.
	 */
	public static final String BADREQUEST_MESSAGE = "La petici\u00F3n recibida no es valida";

	/**
	 * NOTFOUND_MESSAGE.
	 */
	public static final String NOTFOUND_MESSAGE = "No se han podido encontrar resultados para tu b\u00FAsqueda";

	/**
	 * CONFLICT_MESSAGE.
	 */
	public static final String CONFLICT_MESSAGE = "Conflicto al procesar la petici\u00F3n";

	/**
	 * Constante numero 3
	 */
	public static final int TRES = 3;

	/**
	 * Constante numero 8
	 */
	public static final int NUEVE = 9;

}

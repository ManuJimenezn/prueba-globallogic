package com.examen.manueljimenez.creacionusuario.entity;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * Clase que representa la Tabla Clientes de la Base de datos.
 * 
 * @author Manuel Jimenez
 */
@Data
@Entity
@Table(name = "Usuarios")
public class UsuarioEntity implements Serializable {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id del usuario.
	 */
	@Id
	@Column(name = "ID_USUARIO")
	private UUID idCliente;

	/**
	 * nombre del cliente.
	 */
	@Column(name = "NOMBRE_USUARIO", length = 255)
	private String nombreCliente;

	/**
	 * nombre del cliente.
	 */
	@Column(name = "EMAIL", length = 255)
	private String email;

	/**
	 * Password;
	 */
	@Column(name = "PASSWORD", length = 50)
	private String password;

	/**
	 * Fecha Creacion Cliente.
	 */
	@Column(name = "FECHA_CREACION")
	private Timestamp fechaCreacion;

	/**
	 * Fecha ultima Actualizacion Cliente.
	 */
	@Column(name = "FECHA_ACTUALIZACION")
	private Timestamp fechaActualizacion;

	/**
	 * Fecha ultimo ingreso.
	 */
	@Column(name = "LAST_LOGIN")
	private Timestamp fechaUltimoIngreso;

	/**
	 * Token Cliente.
	 */
	@Column(name = "TOKEN")
	private String token;

	/**
	 * Estado del cliente.
	 */
	@Column(name = "ISACTIVE")
	private Boolean estadoCliente;

}

package com.examen.manueljimenez.creacionusuario.entity;

import java.io.Serializable;
import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

import lombok.Data;

/**
 * Clase que representa una entidad de Contacto.
 * 
 * @author Manuel Jimenez
 */
@Data
@Entity
@IdClass(ContactoEntityKey.class)
@Table(name = "Clientes")
public class ContactoEntity implements Serializable {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id del cliente.
	 */
	@Id
	@Column(name = "ID_USUARIO")
	private UUID idUsuario;

	/**
	 * Numero Contacto
	 */
	@Id
	@Column(name = "NUMERO", length = 8)
	private Long numero;

	/**
	 * Codigo de la Ciudad.
	 */
	@Column(name = "CODIGO_CIUDAD", length = 3)
	private Long codigoCiudad;

	/**
	 * Codigo del Pais.
	 */
	@Column(name = "CODIGO_PAIS", length = 3)
	private Long codigoPais;
}
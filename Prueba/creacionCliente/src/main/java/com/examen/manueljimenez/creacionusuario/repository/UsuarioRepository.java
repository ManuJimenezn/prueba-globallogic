package com.examen.manueljimenez.creacionusuario.repository;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.examen.manueljimenez.creacionusuario.entity.UsuarioEntity;

/**
 * Interface que administra la persistencia para ClientesEntity.
 * 
 * @author Manuel Jimenez.
 */
public interface UsuarioRepository extends JpaRepository<UsuarioEntity, UUID> {

}

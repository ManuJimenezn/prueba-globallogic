package com.examen.manueljimenez.creacionusuario.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;

import com.examen.manueljimenez.creacionusuario.entity.ContactoEntity;

/**
 * Interface que administra la persistencia para ContactoEntity.
 * 
 * @author Manuel Jimenez.
 */
public interface ContactoRepository extends CrudRepository<ContactoEntity, UUID> {

}

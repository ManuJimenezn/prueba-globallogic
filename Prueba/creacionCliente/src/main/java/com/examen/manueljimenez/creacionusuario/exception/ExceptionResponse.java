package com.examen.manueljimenez.creacionusuario.exception;

import java.io.Serializable;

import lombok.Data;

/**
 * Respuesta de excepcion.
 * 
 * @author Manuel Jimenez.
 */
@Data
public class ExceptionResponse implements Serializable {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Message.
	 */
	private String message;

	/**
	 * Constructor.
	 * 
	 * @param message message.
	 */
	public ExceptionResponse(String message) {

		super();
		this.message = message;
	}
}

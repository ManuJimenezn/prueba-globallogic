package com.examen.manueljimenez.creacionusuario.util;

import java.security.Key;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;

import com.examen.manueljimenez.creacionusuario.exception.InvalidRequestException;

import io.jsonwebtoken.JwtBuilder;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Clase que contiene el utilitario del proyecto.
 * 
 * @author Manuel Jimenez
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class UsuarioUtil {

	/**
	 * Pattern Password.
	 */
	public static final Pattern PATTERN_PASSWORD = Pattern.compile("^.*(?=.*[a-z])(?=.*[A-Z]).*$");

	/**
	 * Pattern Email.
	 */
	public static final Pattern PATTERN_EMAIL = Pattern
			.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");

	/**
	 * Tiempo para Token.
	 */
	private static final Long TIME = 1800000L;

	/**
	 * Key para Token.
	 */
	private static final String SECRET_KEY = "oeRaYY7Wo24sDqKSX3IM9ASGmdGPmkTd9jo1QTy4b7P9Ze5_9hKolVX8xNrQDcNRfVEdTZNOuOyqEGhXEbdJI-ZQ19k_o9MI0y3eZN2lp9jow55FfXMiINEdt1XR85VipRLSOkT6kSpzs2x-jbLDiz9iFVzkd81YKxMgPA7VfZeQUm4n-mOmnWMaVX30zGFU4L3oPBctYKkl4dYfqYWqRNfrgPJVi5DGFjywgxx0ASEiJHtV72paI3fDR2XwlSkyhhmY-ICjCRmsJN4fX1pdoL8a18-aQrvyu4j0Os6dVPYIoPvvY0SAZtWYKHfM15g7A3HD4cVREf9cUsprCRK93w";

	/**
	 * Lista nula o vacia
	 */
	private static final String ES_NULO_LIST = "[esNuloListRequest]";

	/**
	 * Evalua si el valor es null o vacio.
	 * 
	 * @param texto    texto.
	 * @param message  message.
	 * @param solution solution.
	 * @param code     code.
	 * @return text.
	 * @throws InvalidRequestException si objeto es nulo o vacio
	 */
	public static String esNuloOVacioRequest(String texto, String message) throws InvalidRequestException {

		return Optional.ofNullable(texto).filter(a -> !ConstanteUsuario.EMPTY.equals(a))
				.orElseThrow(() -> new InvalidRequestException("[esNuloOVacio]".concat(message)));
	}

	/**
	 * Evalua si el primer valor de la lista es null.
	 * 
	 * @param lista   lista a evaluar.
	 * @param mensaje mensaje respuesta
	 * @return primer elemento de la lista si no es nulo.
	 * @throws InvalidResponseException si el objeto es nulo.
	 */
	public static <T> T esNuloListRequest(List<T> lista, String mensaje) throws InvalidRequestException {

		return Optional.ofNullable(lista).orElseThrow(() -> new InvalidRequestException(ES_NULO_LIST.concat(mensaje)))
				.stream().findFirst().orElseThrow(() -> new InvalidRequestException(ES_NULO_LIST.concat(mensaje)));

	}

	/**
	 * Obtiene la fecha con el formato yyyy-MM-dd.
	 * 
	 * @return Fecha formateada [yyyy-MM-dd]
	 */
	public static Timestamp getDate() {

		return new Timestamp(new Date().getTime());
	}

	/**
	 * Metodo que valida el formato del email..
	 * 
	 * @param string string a validar.
	 * @return true or false.
	 */
	public static boolean validarEmail(String email) {

		Matcher mather = PATTERN_EMAIL.matcher(email);

		boolean valido = false;
		if (mather.find()) {
			valido = true;
		}
		return valido;
	}

	/**
	 * validador de password.
	 * 
	 * @param passsword.
	 * @return boolean.
	 */
	public static boolean validaPassword(String passsword) {
		if (passsword == null || passsword.isEmpty()) {
			return false;
		}
		String passNumber = passsword.concat("fin");
		String[] split = passNumber.split("[0-9]");

		boolean valido = false;

		if (PATTERN_PASSWORD.matcher(passsword).find() && split.length > 2) {
			valido = true;
		}
		return valido;
	}

	/**
	 * Metodo que crear un token con jwt.
	 * 
	 * @param id      UUID
	 * @param issuer  usuario System.
	 * @param subject nombre Usuario.
	 * @return
	 */
	public static String createJWT(String id, String issuer, String subject) {

		SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;

		long nowMillis = System.currentTimeMillis();
		Date now = new Date(nowMillis);

		byte[] apiKeySecretBytes = DatatypeConverter.parseBase64Binary(SECRET_KEY);
		Key signingKey = new SecretKeySpec(apiKeySecretBytes, signatureAlgorithm.getJcaName());

		JwtBuilder builder = Jwts.builder().setId(id).setIssuedAt(now).setSubject(subject).setIssuer(issuer)
				.signWith(signatureAlgorithm, signingKey);

		if (TIME > 0) {
			long expMillis = nowMillis + TIME;
			Date exp = new Date(expMillis);
			builder.setExpiration(exp);
		}

		return builder.compact();
	}
}

package com.examen.manueljimenez.creacionusuario.service;

import java.util.List;
import java.util.UUID;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.examen.manueljimenez.creacionusuario.entity.ContactoEntity;
import com.examen.manueljimenez.creacionusuario.entity.UsuarioEntity;
import com.examen.manueljimenez.creacionusuario.exception.BadRequestException;
import com.examen.manueljimenez.creacionusuario.exception.ConflictException;
import com.examen.manueljimenez.creacionusuario.exception.InvalidRequestException;
import com.examen.manueljimenez.creacionusuario.model.Phone;
import com.examen.manueljimenez.creacionusuario.model.Usuario;
import com.examen.manueljimenez.creacionusuario.repository.ContactoRepository;
import com.examen.manueljimenez.creacionusuario.repository.UsuarioRepository;
import com.examen.manueljimenez.creacionusuario.util.ConstanteUsuario;
import com.examen.manueljimenez.creacionusuario.util.UsuarioUtil;

import lombok.extern.log4j.Log4j2;

/**
 * Clase que se encarga de tener el negocio para crear al cliente.
 * 
 * @author Manuel Jimenez
 */
@Service
@Log4j2
public class ClienteService {

	/**
	 * Instancia de ClienteRepository.
	 */
	@Autowired
	private UsuarioRepository usuarioRepository;

	/**
	 * Instancia de ContactoRepository.
	 */
	@Autowired
	private ContactoRepository contactoRepository;

	/**
	 * Metodo que permite procesar los pasos de crear al usuario.
	 * 
	 * @param cliente
	 * @return Objeto Cliente.
	 */
	public UsuarioEntity procesarUsuario(Usuario usuario) {

		log.debug("[procesarCliente][{}] Inicio. Nombre Cliente", usuario.getName());

		UsuarioEntity nuevoUsuario = new UsuarioEntity();
		try {
			validarUsuario(usuario);
			nuevoUsuario = crearUsuario(usuario);
		} catch (InvalidRequestException e) {
			log.error("[prepararCliente]Error. un dato dentro del Objeto es null o vacio.");
			throw new BadRequestException(e.getMessage());
		}

		log.debug("[procesarCliente][{}] Inicio. Nombre Cliente", usuario.getName());
		return nuevoUsuario;
	}

	/**
	 * Metodo que se encarga de validar si el correo del usuario ya existe.
	 * 
	 * @param cliente.
	 * @throws InvalidRequestException
	 */
	private void validarUsuario(Usuario usuario) throws InvalidRequestException {

		log.debug("[ValidarCorreoUsuario][{}] Inicio. Nombre Cliente", usuario.getName());

		UsuarioUtil.esNuloOVacioRequest(usuario.getEmail(), "Error. el email no puede ser null o vacio.");
		List<UsuarioEntity> clientesEntity = usuarioRepository.findAll();
		Predicate<UsuarioEntity> matchEmail = usuarios -> usuarios.getEmail().equals(usuario.getEmail());
		if (!clientesEntity.isEmpty() && clientesEntity.stream().anyMatch(matchEmail)) {
			log.error("[ValidarCorreoUsuario][{}] Error. Correo del Cliente ya se encuentra registrado",
					usuario.getEmail());
			throw new ConflictException("Error su correo ya se encuentra en nuestros registros.");
		}

		if (!UsuarioUtil.validarEmail(usuario.getEmail())) {
			log.error("[ValidarCorreoUsuario][{}] Error. Correo no valido", usuario.getEmail());
			throw new BadRequestException("Error correo no valido.");
		}

		if (!UsuarioUtil.validaPassword(usuario.getPassword())) {
			log.error("[crearUsuario][{}] La Contraseņa: ", usuario.getPassword(), " es invalida");
			throw new BadRequestException("La contraseņa es invalida");
		}

		UsuarioUtil.esNuloListRequest(usuario.getPhones(), "Error. el listado de contacto no puede ser null o vacio.");
		
		Predicate<Phone> matchPhone = phone -> phone.getCitycode().toString().length() > ConstanteUsuario.TRES
				|| phone.getNumber().toString().length() > ConstanteUsuario.NUEVE
				|| phone.getContrycode().toString().length() > ConstanteUsuario.TRES;

		if (usuario.getPhones().stream().anyMatch(matchPhone)) {
			throw new BadRequestException("El codigo de ciudad, pais o numero es mas largo de lo permitido.");
		}

		log.debug("[ValidarCorreoUsuario][{}] Fin. Nombre Cliente", usuario.getName());
	}

	/**
	 * Metodo encargado de crear al usuario.
	 * 
	 * @param Object usuario
	 * @return
	 * @throws InvalidRequestException
	 */
	private UsuarioEntity crearUsuario(Usuario usuario) throws InvalidRequestException {

		log.debug("[crearUsuario][{}] Inicio. Usuario", usuario.getName());

		UsuarioEntity clienteEntity = prepararCliente(usuario);
		List<ContactoEntity> contactoCliente = prepararContacto(usuario, clienteEntity.getIdCliente());
		usuarioRepository.save(clienteEntity);
		persisitirContacto(contactoCliente);
		log.info("[crearUsuario] Usuario Creado exitosamente");
		log.debug("[crearUsuario][{}] Fin. Usuario", usuario.getName());
		return clienteEntity;
	}

	/**
	 * Metodo que se encargas de crear la entidad ClientesEntity para ser
	 * persisitida.
	 * 
	 * @param usuario
	 * @return ClientesEntity
	 * @throws InvalidRequestException
	 */
	private UsuarioEntity prepararCliente(Usuario usuario) throws InvalidRequestException {

		log.debug("[prepararCliente] Inicio.");

		Long countUserDB = usuarioRepository.count();
		UUID idUser = UUID.nameUUIDFromBytes(usuario.getEmail().concat(countUserDB.toString()).getBytes());

		UsuarioEntity nuevoCliente = new UsuarioEntity();
		nuevoCliente.setIdCliente(idUser);
		nuevoCliente.setNombreCliente(
				UsuarioUtil.esNuloOVacioRequest(usuario.getName(), "Nombre cliente no puede ser nulo o vacio."));
		nuevoCliente.setEmail(
				UsuarioUtil.esNuloOVacioRequest(usuario.getEmail(), "Email cliente no puede ser nulo o vacio."));
		nuevoCliente.setPassword(
				UsuarioUtil.esNuloOVacioRequest(usuario.getPassword(), "Password cliente no puede ser nulo o vacio."));
		nuevoCliente.setFechaCreacion(UsuarioUtil.getDate());
		nuevoCliente.setFechaActualizacion(UsuarioUtil.getDate());
		nuevoCliente.setFechaUltimoIngreso(UsuarioUtil.getDate());
		nuevoCliente.setToken(UsuarioUtil.createJWT(idUser.toString(), "SYSTEM", usuario.getName()));
		nuevoCliente.setEstadoCliente(true);
		log.debug("[prepararCliente] Fin.");
		return nuevoCliente;
	}

	/**
	 * Metodo function que contiene la logica para armar el listado de
	 * ContactoEntity.
	 * 
	 * @param idUsuario
	 * @return
	 */
	private Function<Phone, ContactoEntity> mapListadoContato(UUID idUsuario) {
		return contacto -> {
			ContactoEntity contactoEntity = new ContactoEntity();
			contactoEntity.setIdUsuario(idUsuario);
			contactoEntity.setNumero(contacto.getNumber());
			contactoEntity.setCodigoCiudad(contacto.getCitycode());
			contactoEntity.setCodigoPais(contacto.getContrycode());
			return contactoEntity;
		};
	}

	/**
	 * Metodo que construye ContactoEntity
	 * 
	 * @param usuario   Object Usuario
	 * @param idUsuario UUID
	 * @return List ContactoEntity.
	 */
	private List<ContactoEntity> prepararContacto(Usuario usuario, UUID idUsuario) {

		return usuario.getPhones().stream().map(mapListadoContato(idUsuario)).collect(Collectors.toList());
	}

	/**
	 * Metodo que se encarga de persistir los contactos del Usuario.
	 * 
	 * @param contactoCliente List ContactoEntity.
	 */
	private void persisitirContacto(List<ContactoEntity> contactoCliente) {

		contactoCliente.stream().forEach(contactoEntity -> contactoRepository.save(contactoEntity));

	}
}

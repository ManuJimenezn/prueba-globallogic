package com.examen.manueljimenez.creacionusuario.controller;

import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.examen.manueljimenez.creacionusuario.exception.ExceptionHandlerAdviceBase;

/**
 * Controller que maneja las excepciones lanzadas por la aplicacion.
 * 
 * @author Manuel Jimenez.
 */
@RestControllerAdvice
public class ExceptionHandlerController extends ExceptionHandlerAdviceBase {

}

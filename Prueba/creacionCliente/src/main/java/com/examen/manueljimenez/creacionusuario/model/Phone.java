package com.examen.manueljimenez.creacionusuario.model;

import java.io.Serializable;

import lombok.Data;

/**
 * Modelo que se encarga de representar a un Contacto.
 * 
 * @author Manuel Jimenez.
 */
@Data
public class Phone implements Serializable {

	/**
	 * Serial
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Numero de telefono
	 */
	private Long number;

	/**
	 * Codigo de cuidad
	 */
	private Long citycode;

	/**
	 * Codigo de Pais
	 */
	private Long contrycode;
}

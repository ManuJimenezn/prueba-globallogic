package com.examen.manueljimenez.creacionusuario.model;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

/**
 * Modelo que se encarga de representar a un Cliente.
 * 
 * @author Manuel Jimenez.
 */
@Data
public class Usuario implements Serializable {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Nombre.
	 */
	private String name;

	/**
	 * Email.
	 */
	private String email;

	/**
	 * Password
	 */
	private String password;

	/**
	 * Lista de contactos.
	 */
	private List<Phone> phones;
}

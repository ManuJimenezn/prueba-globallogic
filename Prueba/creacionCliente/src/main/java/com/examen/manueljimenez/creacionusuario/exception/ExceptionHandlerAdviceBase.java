package com.examen.manueljimenez.creacionusuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.examen.manueljimenez.creacionusuario.util.ConstanteUsuario;

import lombok.extern.log4j.Log4j2;

/**
 * Controlador generico para Administrar la excepcion de respuesta.
 * 
 * @author Manuel Jimenez.
 */
@Log4j2
public class ExceptionHandlerAdviceBase extends ResponseEntityExceptionHandler {

	/**
	 * Metodo que retorna una excepcion de BadRequestException.
	 * 
	 * @param ex      BadRequestException.
	 * @param request WebRequest
	 * @return
	 */
	@ExceptionHandler(BadRequestException.class)
	public final ResponseEntity<ExceptionResponse> handleBadRequestException(BadRequestException ex,
			WebRequest request) {

		ExceptionResponse exceptionResponse = null;
		if (ex.getMessage() != null) {
			log.error("Exception handler BadRequestException", ex.getMessage());
			exceptionResponse = new ExceptionResponse(ex.getMessage());
		} else {
			exceptionResponse = new ExceptionResponse(ConstanteUsuario.BADREQUEST_MESSAGE);
		}
		return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
	}

	/**
	 * Metodo que retorna una excepcion de ConflictException.
	 * 
	 * @param ex      ConflictException
	 * @param request WebRequest
	 * @return
	 */
	@ExceptionHandler(ConflictException.class)
	public final ResponseEntity<ExceptionResponse> handleConflictException(ConflictException ex, WebRequest request) {

		ExceptionResponse exceptionResponse = null;
		if (ex.getMessage() != null) {
			log.error("Exception handler ConflictException", ex.getMessage());
			exceptionResponse = new ExceptionResponse(ex.getMessage());
		} else {
			exceptionResponse = new ExceptionResponse(ConstanteUsuario.CONFLICT_MESSAGE);
		}
		return new ResponseEntity<>(exceptionResponse, HttpStatus.CONFLICT);
	}

}

package com.examen.manueljimenez.creacionusuario.entity;

import java.io.Serializable;
import java.util.UUID;

import lombok.Data;

/**
 * Entidad que contiene las llaves de la entidad ContatoEntity.
 * 
 * @author Manuel Jimenez
 */
@Data
public class ContactoEntityKey implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Id del usuario.
	 */
	private UUID idUsuario;

	/**
	 * Numero de telefono.
	 */
	private Long numero;
}

package com.examen.manueljimenez.creacionusuario.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

import lombok.Getter;

/**
 * ConflictException
 * 
 * @author Manuel Jimenez.
 */
@Getter
@ResponseStatus(code = HttpStatus.CONFLICT)
public class ConflictException extends RuntimeException {

	/**
	 * Serial.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Empty Constructor.
	 */
	public ConflictException() {

		super();
	}

	/**
	 * Constructor.
	 *
	 * @param message message.
	 */
	public ConflictException(String message) {

		super(message);
	}

}
package com.examen.manueljimenez.creacionusuario.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * Clase que maneja el error de request invalidos.
 * 
 * @author Manuel Jimenez
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class InvalidRequestException extends Exception {

	/**
	 * Serial de la clase.
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Constructor con parametros.
	 * 
	 * @param message mensaje.
	 */
	public InvalidRequestException(String message) {

		super(message);
	}
}

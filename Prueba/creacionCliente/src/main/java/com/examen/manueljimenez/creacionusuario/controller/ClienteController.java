package com.examen.manueljimenez.creacionusuario.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.examen.manueljimenez.creacionusuario.entity.UsuarioEntity;
import com.examen.manueljimenez.creacionusuario.model.Usuario;
import com.examen.manueljimenez.creacionusuario.service.ClienteService;

import lombok.extern.log4j.Log4j2;

/**
 * Controller encargado de recepcionar las peticions rest.
 * 
 * @author Manuel Jimenez
 */
@Log4j2
@RestController
@RequestMapping("/cliente-manager")
public class ClienteController {

	/**
	 * Instancia de ClienteServices
	 */
	@Autowired
	private ClienteService clienteService;

	/**
	 * Metodo que se encarga de crear a un usuario en bd.
	 * 
	 * @param cliente
	 * @return ResponseEntity Usuario.
	 */
	@PostMapping("/crearUsuario")
	public ResponseEntity<UsuarioEntity> crearUsuario(@RequestBody Usuario cliente) {

		log.info("[crearUsuario] Inicio");
		UsuarioEntity response = clienteService.procesarUsuario(cliente);
		log.info("[crearUsuario] Fin");
		return new ResponseEntity<>(response, HttpStatus.OK);
	}
}

CREATE OR REPLACE TABLE Contacto(
  ID_USUARIO uuid PRIMARY KEY,
  NUMERO Long(8) NOT NULL ,
  CODIGO_CIUDAD Long(3) NOT NULL,
  CODIGO_PAIS Long(3) NOT NULL
);

CREATE OR REPLACE TABLE Usuarios (
  ID_USUARIO uuid PRIMARY KEY,
  NOMBRE_USUARIO VARCHAR(255) NOT NULL,  
  EMAIL VARCHAR(250) NOT NULL,
  PASSWORD VARCHAR(50) NOT NULL,
  FECHA_CREACION TIMESTAMP,
  FECHA_ACTUALIZACION TIMESTAMP,
  LAST_LOGIN TIMESTAMP,
  TOKEN VARCHAR(255),
  ISACTIVE Boolean
);